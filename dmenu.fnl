#!/usr/bin/env fennel
(local dmenu
       {
        :presets {}
        })



(fn dmenu.presets.centered [args]
  (local arguments
         {
          :center true
          :lines   5
          :columns 2
          }
         )
  (each [key value (pairs args)]
    (tset arguments key value)
    )
  arguments
  )

(fn dmenu.presets.bottom [args]
  (var arguments
       {
        :lines 5
        }
       )
  (each [key value (pairs args)]
    (tset arguments key value)
    )
  (tset arguments :bottom true)
  arguments
  )

(fn dmenu.show [input args]
  "takes 2 arguments(tables)
input(should contain the input values)
and
args (can be based on one of the items in the templates table and should be formated like that) "
  (fn argsBuilder [args]
    (local prompt  (or (.. "-p '" args.prompt "'") " "))
    (local bottom  (if args.bottom "-b" " "))
    (local center  (if args.center "-c" " "))
    (local lines   (if args.lines (.. "-l " args.lines ) " "))
    (local columns (if args.columns (.. "-g " args.columns ) " "))
    (local instant (if args.instant "-n " " "))
    (local separator (if args.separator (.. "-d " args.separator) " "))
    (table.concat [prompt bottom center lines columns instant separator] " ")
    )
(.. "echo \"" (table.concat input (if args.insep args.insep "\n")) "\"|dmenu " (argsBuilder args))
  )




(fn dmenu.capture [cmd]
  (local f (assert (io.popen cmd :r)))
  (var s (assert (f:read :*a)))
  (f:close)
  (local raw s)
  (set s (string.gsub s "^%s+" ""))
  (set s (string.gsub s "%s+$" ""))
  (set s (string.gsub s "[\n\r]+" " "))
  {:out s
   :raw raw}
  )




dmenu
